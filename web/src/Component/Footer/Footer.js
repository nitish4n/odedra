import React from 'react'
import './footer.css'
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import YouTubeIcon from '@material-ui/icons/YouTube';
import InstagramIcon from '@material-ui/icons/Instagram';

function Footer() {
    return (
        <footer className="ct-footer">
            <div className="container">
                {/* <form name="contentForm" enctype="multipart/form-data" method="post" action="">
                    <div className="ct-footer-pre text-center-lg">
                        <div className="inner">
                        <span>Join WebCorpCo to receive updates, news & events!</span>
                        </div>
                        <div className="inner">
                        <div className="form-group">
                            <input name="formfields[nl_email]" id="nl_email" className="validate[required]" placeholder="Enter your email address" type="text" value="" /> <label for="nl_email" className="sr-only">Email Address</label> <button type="submit" className="btn btn-motive">Join</button>
                        </div>
                        </div>
                    </div>
                </form> */}
                {/* <ul className="ct-footer-list text-center-sm">
                <li>
                    <h2 className="ct-footer-list-header">Learn More</h2>
                    <ul>
                    <li>
                        <a href="">Company</a>
                    </li>
                    <li>
                        <a href="">Clients</a>
                    </li>
                    <li>
                        <a href="">News</a>
                    </li>
                    <li>
                        <a href="">Careers</a>
                    </li>
                    </ul>
                </li>
                <li>
                    <h2 className="ct-footer-list-header">Services</h2>
                    <ul>
                    <li>
                        <a href="">Design</a>
                    </li>
                    <li>
                        <a href="">Marketing</a>
                    </li>
                    <li>
                        <a href="">Sales</a>
                    </li>
                    <li>
                        <a href="">Programming</a>
                    </li>
                    <li>
                        <a href="">Support</a>
                    </li>
                    </ul>
                </li>
                {/* <li>
                    <h2 className="ct-footer-list-header">The Industry</h2>
                    <ul>
                    <li>
                        <a href="">Thought Leadership</a>
                    </li>
                    <li>
                        <a href="">Webinars</a>
                    </li>
                    <li>
                        <a href="">Events</a>
                    </li>
                    <li>
                        <a href="">Sponsorships</a>
                    </li>
                    <li>
                        <a href="">Advisors</a>
                    </li>
                    <li>
                        <a href="">Training Program</a>
                    </li>
                    <li>
                        <a href="">Activities & Campaigns</a>
                    </li>
                    </ul>
                </li> }
                <li>
                    <h2 className="ct-footer-list-header">Public Relations</h2>
                    <ul>
                    <li>
                        <a href="">WebCorpCo Blog</a>
                    </li>
                    <li>
                        <a href="">Hackathons</a>
                    </li>
                    <li>
                        <a href="">Videos</a>
                    </li>
                    <li>
                        <a href="">News Releases</a>
                    </li>
                    <li>
                        <a href="">Newsletters</a>
                    </li>
                    </ul>
                </li>
                <li>
                    <h2 className="ct-footer-list-header">About</h2>
                    <ul>
                    <li>
                        <a href="">FAQ</a>
                    </li>
                    <li>
                        <a href="">Our Board</a>
                    </li>
                    <li>
                        <a href="">Our Staff</a>
                    </li>
                    <li>
                        <a href="">Contact Us</a>
                    </li>
                    </ul>
                </li>
                </ul> */}
                <div className="ct-footer-meta text-center-sm">
                <div className="row">

                    <div className="col-xs-6 col-sm-6 col-md-9">
                        <address>
                            <span>Odedra Studio<br /></span>123 Easy Street<br />
                            Mumbai , 400059<br />
                            <span>Phone: <a href="tel:5555555555">+91 9876543210</a></span>
                        </address>
                    </div>
                    
                    <div className="col-xs-6 col-sm-6 col-md-3">
                        <ul className="ct-socials list-unstyled list-inline social__icons">
                            <li>
                            <a href="" target="_blank">
                                <FacebookIcon />
                            </a>
                            </li>
                            <li>
                            <a href="" target="_blank">
                                <TwitterIcon />
                            </a>
                            </li>
                            <li>
                            <a href="" target="_blank">
                                <YouTubeIcon />
                                </a>
                            </li>
                            <li>
                            <a href="" target="_blank">
                                <InstagramIcon />
                            </a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                </div>
            </div>
            <div className="ct-footer-post">
                <div className="container">
                <div className="inner-left">
                    <ul>
                    <li>
                        <a href="">Clients</a>
                    </li>
                    <li>
                        <a href="">Services</a>
                    </li>
                    <li>
                        <a href="">Contact Us</a>
                    </li>
                    </ul>
                </div>
                <div className="inner-right">
                    <p>Copyright © 2020 Odedra Studio&nbsp;<a href="">Privacy Policy</a></p>
                    <p><a className="ct-u-motive-color" href="" target="_blank">Developed</a> by <a href="" target="_blank">devAdda.com </a></p>
                </div>
                </div>
            </div>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            </footer>
    )
}

export default Footer
