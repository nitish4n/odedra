import React from 'react'
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import MailIcon from '@material-ui/icons/Mail';
import './team.css';
function Index() {
    return (
        <div className="text_details">
                <h1>Meet Team</h1>
                <p>Perfect Product get deievered by perfect team</p>
                <div className="team_data">
                    <div className="owl-item">
                        <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div className="single-team">
                                <img src="https://envytheme.com/tf-jsx-demo/exolot/static/media/team-img3.3b7d815b.jpg" alt="team-img" />
                                <ul className="member-social-links wow fadeInUp">
                                    <li><a href="#" className="icofont-facebook"><FacebookIcon /> </a></li>
                                    <li><a href="#" className="icofont-instagram"><InstagramIcon /> </a></li>
                                    <li><a href="#" className="icofont-linkedin"><MailIcon /> </a></li>
                                </ul>
                                <h3>Maxwel Smith</h3>
                                <span>Web Developer</span>
                            </div>
                        </div>
                    </div>
                    <div className="owl-item" style={{width: "285px;"}}>
                        <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div className="single-team">
                                <img src="https://envytheme.com/tf-jsx-demo/exolot/static/media/team-img1.daf0765d.jpg" alt="team-img" />
                                <ul className="member-social-links wow fadeInUp">
                                    <li><a href="#" className="icofont-facebook"><FacebookIcon /> </a></li>
                                    <li><a href="#" className="icofont-instagram"><InstagramIcon /> </a></li>
                                    <li><a href="#" className="icofont-linkedin"><MailIcon /> </a></li>
                                </ul>
                                <h3>Maxwel Smith</h3>
                                <span>Web Developer</span>
                            </div>
                        </div>
                    </div>
                    <div className="owl-item" style={{width: "285px;"}}>
                        <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div className="single-team">
                                <img src="https://envytheme.com/tf-jsx-demo/exolot/static/media/team-img2.fd9d1b55.jpg" alt="team-img" />
                                <ul className="member-social-links wow fadeInUp">
                                    <li><a href="#" className="icofont-facebook"><FacebookIcon /> </a></li>
                                    <li><a href="#" className="icofont-instagram"><InstagramIcon /> </a></li>
                                    <li><a href="#" className="icofont-linkedin"><MailIcon /> </a></li>
                                </ul>
                                <h3>Maxwel Smith</h3>
                                <span>Web Developer</span>
                            </div>
                        </div>
                    </div>
                    <div className="owl-item" style={{width: "285px;"}}>
                        <div className="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div className="single-team">
                                <img src="https://envytheme.com/tf-jsx-demo/exolot/static/media/team-img4.a2c6b8c9.jpg" alt="team-img" />
                                <ul className="member-social-links wow fadeInUp">
                                    <li><a href="#" className="icofont-facebook"><FacebookIcon /> </a></li>
                                    <li><a href="#" className="icofont-instagram"><InstagramIcon /> </a></li>
                                    <li><a href="#" className="icofont-linkedin"><MailIcon /> </a></li>
                                </ul>
                                <h3>Maxwel Smith</h3>
                                <span>Web Developer</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default Index
