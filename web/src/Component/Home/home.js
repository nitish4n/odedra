import React, {Fragment} from 'react'
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import FacebookIcon from '@material-ui/icons/Facebook';
import YouTubeIcon from '@material-ui/icons/YouTube';
import InstagramIcon from '@material-ui/icons/Instagram';
import {Tween,Timeline } from 'react-gsap';
import './home.css';
function home() {
    return (
        <Fragment>
            <Tween to={{ delay: 1, position:"absolute" , y:-1160}} duration={4} ease="in.out(1.7)">
          <div className="overlay">
            <h1>Odedra Studio</h1>
            <span>A Media & development company</span>
          </div>
        </Tween>
        <div className="wrapper">
          <div className="nav">
            <Tween from={{y:-100, display:"none", opacity:1 , delay: 3 ,}} duration={5} ease="back.out(1.7)">
              <div className="logo">
                <h1>
                  <span>Odedra <br /> Studio</span>
                  <br />
                  Media Studio
                </h1>
              </div>
            </Tween>
            <div className="menu__link">
              <ul>
                
            <Tween from={{ x:200 }} stagger={0.4} ease="elastic.out(0.1, 0.1)">
                <li>Home</li>
                <li>Course</li>
                <li>Services</li>
                <li>About Us</li>
                <li>Our Works</li>
                <li>Conatct Us</li>
              </Tween>
              </ul>
            </div>

            <div className="scroll__down">
              Scroll
            </div>
          </div>

          {/* <div className="text">
            <div className="title">
              We are
            </div>
            <p>
              In handle Key Press() function, I check the keyCode to determine which key was pressed by user.
            </p>
          </div> */}


          <Timeline
            target={
              <div className="watch__now">
                  <img alt="" className="odedra__boy" src="https://res.cloudinary.com/nitish4n/image/upload/v1600893212/c_00025_gmwthc.png" width="200px" height="auto" />
                <PlayArrowIcon />
                <a href="/">Watch Now</a>
              </div>
            }
          >
            <Tween from={{ opacity: 0 }} to={{ opacity: 1 }} duration={3} stagger={1} />
            <Tween from={{ display:"none" }} stagger={1} />

            </Timeline>

          <div className="media">
            <ul>
              <li>
                <FacebookIcon />
              </li>

              <li><YouTubeIcon /></li>
              <li><InstagramIcon /></li>
              <li></li>
            </ul>
          </div>

        </div>
        {/* nav closed */}

        <div className="ellipse-container">
            <div className="ellipse thin"></div>
            <div className="ellipse thick"></div>
            <div className="ellipse yellow"></div>
            <div className="circle1"><span>Learn More here</span></div>
            <div className="circle2"><span>We are here</span></div>
          </div>
        </Fragment>
    )
}

export default home
