import React, { useEffect, useState } from "react";
import styled from "styled-components";
import {Link} from 'react-router-dom';
import './header.css'
const Nav = styled.nav`
  padding: 0 20px;
  min-height: 9vh;

  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Logo = styled.h1`
  font-size: 25px;
  color: white;
`;

const Menu = styled.ul`
  list-style: none;
  display: flex;

  li {
    margin: 0px 20px;
  }

  @media (max-width: 768px) {
    display: none;
  }
`;

const Item = styled.li``;

const NavIcon = styled.button`
  background: #19ae8e;
  cursor: pointer;
  border: none;
  outline: none;

  @media (min-width: 769px) {
    display: none;
  }
`;

const Line = styled.span`
  display: block;
  border-radius: 50px;
  width: 25px;
  height: 3px;
  margin: 5px;
  background-color: #fff;
  transition: width 0.4s ease-in-out;

  :nth-child(2) {
    width: ${props => (props.open ? "40%" : "70%")};
  }
`;

const Overlay = styled.div`
  position: absolute;
  height: ${props => (props.open ? "91vh" : 0)};
  width: 100vw;
  background: #42275a;  /* fallback for old browsers */
  background: -webkit-linear-gradient(to right, #734b6d, #42275a);  /* Chrome 10-25, Safari 5.1-6 */
  background: linear-gradient(to right, #734b6d, #42275a); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

  transition: height 0.4s ease-in-out;
  z-index:9999;

  @media (min-width: 769px) {
    display: none;
  }
`;

const OverlayMenu = styled.ul`
  list-style: none;
  position: absolute;
  left: 50%;
  top: 45%;
  transform: translate(-50%, -50%);

  li {
    opacity: ${props => (props.open ? 1 : 0)};
    font-size: 25px;
    margin: 50px 0px;
    transition: opacity 0.4s ease-in-out;
  }

  li:nth-child(5) {
    margin: 50px 0px;
  }
`;

const Header = ({scrollProperty}) => {
  const [toggle, toggleNav] = useState(false);
  const [headerData , setHeaderData] = useState('inline-flex')
  const handleUrl = (page) => {
    if(page === 'home') {
      setHeaderData('none')
    }else{
      setHeaderData('inline-flex')
    }
  }
  useEffect(() => {
    if(window.location.pathname === '/'){
      if(scrollProperty > 10){
        setHeaderData('inline-flex')
      }else{
      setHeaderData('none')
      }
    } 
    
  }, [scrollProperty])
  return (
    <>
      <Nav className="header"  style={{display: headerData, position:'fixed', zIndex:'9999', backgroundColor: '#000', color:'#D41E5B'}}>
        <Link onClick={() => handleUrl('home')} to="/"><Logo><img 
        className="header__logo" 
        src="https://res.cloudinary.com/nitish4n/image/upload/v1600464511/vijay/2lpgp_rmbkbl.png"
        alt="Odedra Studio"
        /></Logo></Link>
        <Menu>
          <Item>
            <Link onClick={() => handleUrl('home')} to="/">
              Home
            </Link>
          </Item>
          <Item>
            <Link onClick={() => handleUrl('about-us')} to="/about-us">
              About Us
            </Link>
          </Item>
          <Item>
            <Link onClick={() => handleUrl('services')} to="/services">
              Services
            </Link>
          </Item>
          <Item>
            <Link onClick={() => handleUrl('our-work')} to="/our-work">
              Our Work
            </Link>
          </Item>
          <Item>
            <Link onClick={() => handleUrl('contact-us')} to="/contact-us">
              Contact Us
            </Link>
          </Item>
        </Menu>
        <NavIcon onClick={() => toggleNav(!toggle)}>
          <Line open={toggle} />
          <Line open={toggle} />
          <Line open={toggle} />
        </NavIcon>
      </Nav>
      <Overlay open={toggle} className="overlay__menu">
        <OverlayMenu open={toggle}>
          <Item>
            <Link onClick={() => handleUrl('home')} to="/">
            Home
            </Link>
          </Item>
          <Item>
            <Link onClick={() => handleUrl('about-us')} to="/about-us">
            About Us
            </Link>
          </Item>
          <Item>
            <Link onClick={() => handleUrl('services')} to="/services">
            Services
            </Link>
          </Item>
          <Item>
            <Link onClick={() => handleUrl('our-work')} to="/our-work">
            Our Work
            </Link>
          </Item>
          <Item>
            <Link onClick={() => handleUrl('contact-us')} to="/contact-us">
            Contact Us
            </Link>
          </Item>
        </OverlayMenu>
      </Overlay>
    </>
  );
};

export default Header;
