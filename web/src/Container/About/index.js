import React from 'react'
import Team from '../../Component/Team';
import './about.css'
function Index() {

    return (
        <div>
            <link rel="stylesheet" href="https://res.cloudinary.com/odedra/raw/upload/v1601775738/theme_v8sn2e.css" />
            <div className="about-banner">
                <img 
                    src="https://res.cloudinary.com/odedra/image/upload/v1601734858/Youtube_Video_Intro_Template_No_Text_Free_Download_No_Copyright_No_3_lw8eqi.gif"
                    alt="working" 
                />
                <h2>About Us</h2>
            </div>

            <div className="about-details">
                <img 
                    src="https://res.cloudinary.com/odedra/image/upload/v1601746379/web_pc_1_ezwg5z.png"
                    alt="About Odedra Studio"
                />
                <div className="about-detail">
                <h1><b>About Odedra Studio</b></h1>
                <h3>What is Lorem Ipsum?</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more 
                    recently with desktop publishing software like Aldus PageMaker including 
                    versions of Lorem Ipsum.
                </p>
                </div>
            </div>

            <Team />
            <div className="main-container">
    <section className="text-center height-50">
        <div className="container pos-vertical-center">
            <div className="row">
                <div className="col-md-8">
                    <h1>Full-service Design &amp; Development</h1>
                    <p className="lead">
                        From a team of passionate creators working side-by-side with our partners to deliver engaging digital and physical campaigns.
                    </p>
                </div>
            </div>
        
        </div>
    
    </section>
    <section className="imageblock switchable feature-large bg--secondary space--sm">
        <div className="imageblock__content col-lg-6 col-md-4 pos-right">
            <div className="background-image-holder" >
                <img alt="image" src="http://trystack.mediumra.re/img/education-1.jpg" />
            </div>
        </div>
        <div className="container">
            <div className="row">
                <div className="col-lg-5 col-md-7">
                    <h2>Graphic Design</h2>
                    <p className="lead">
                        Stack offers a clean and contemporary to suit a range of purposes from corporate, tech startup, marketing site to digital storefront. Elements have been designed to showcase content in a diverse yet consistent manner.
                    </p>
                </div>
            </div>
        
        </div>
    
    </section>
    <section className="imageblock switchable switchable--switch feature-large bg--secondary space--sm">
        <div className="imageblock__content col-lg-6 col-md-4 pos-right">
            <div className="background-image-holder" >
                <img alt="image" src="http://trystack.mediumra.re/img/inner-5.jpg" />
            </div>
        </div>
        <div className="container">
            <div className="row">
                <div className="col-lg-5 col-md-7">
                    <h2>Brand Marketing</h2>
                    <p className="lead">
                        Multiple font and colour scheme options mean that dramatically altering the look of your site is just clicks away — Customizing your site in the included Variant Page Builder makes experimenting with styles and content arrangements dead simple.
                    </p>
                </div>
            </div>
        
        </div>
    
    </section>
    <section className="imageblock switchable feature-large bg--secondary space--sm">
        <div className="imageblock__content col-lg-6 col-md-4 pos-right">
            <div className="background-image-holder" >
                <img alt="image" src="http://trystack.mediumra.re/img/inner-6.jpg" />
            </div>
        </div>
        <div className="container">
            <div className="row">
                <div className="col-lg-5 col-md-7">
                    <h2>Digital Development</h2>
                    <p className="lead">
                        Stack offers a clean and contemporary to suit a range of purposes from corporate, tech startup, marketing site to digital storefront. Elements have been designed to showcase content in a diverse yet consistent manner.
                    </p>
                </div>
            </div>
        
        </div>
    
    </section>
    
</div>
            
        </div>
    )
}

export default Index
