import React from 'react'
import './contact.css'
function index() {
    return (
        <div className="contact">
            <img
                src="https://www.vramcomputers.com/wp-content/uploads/2014/10/Contact-Us-Page-Banner.jpg"
                alt="odedra studio contact"
            />

            <div className="contact__form">
                <div className="row justify-content-center">
                    <div className="col-lg-6 col-md-offset-3">
                        <div className="section-heading">
                            <h2 className="section-title">Contact Form</h2>
                        </div>
                    </div>
                </div>
                <div className="row align-items-start justify-content-center">
                    <div className="col-md-10 col-md-offset-1">
                        <div className="contact-form-wrap">
                            <div className="empty-form text-center" style={{display: "none"}}><span>Please Fill Required Fields</span></div>
                            <div className="terms-alert text-center" style={{display: "none"}}><span>Please Accept The Terms</span></div>
                            <div className="subject-alert text-center" style={{display: "none"}}><span>Please Select Subject</span></div>
                            <div className="security-alert text-center" style={{display: "none"}}><span>Security code does not match !</span></div>
                            <div className="email-invalid text-center" style={{display: "none"}}><span>Please enter a valid email address.Exp. example@gmail.com</span></div>
                            <div className="phone-invalid text-center" style={{display: "none"}}><span>Please enter a valid phone number.Exp. 200-255-4444</span></div>
                            <form id="contactForm">
                                <div className="row">
                                    <div className="col-lg-6">
                                        <div className="contact-form-group">
                                            <input type="text" className="form-control" name="contact_name" id="contactName" placeholder="Your Name *" />
                                            <i className="far fa-user"></i>
                                            <div className="form-validate-icons">
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="contact-form-group">
                                            <input type="text" className="form-control" name="contact_phone" id="contactPhone" placeholder="Phone Exp. 05555555555 *" />
                                            <i className="fas fa-phone-volume"></i>
                                            <div className="form-validate-icons">
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="contact-form-group">
                                            <input type="text" className="form-control" name="contact_email" id="contactEmail" placeholder="Your Email *" />
                                            <i className="far fa-envelope"></i>
                                            <div className="form-validate-icons">
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-6">
                                        <div className="contact-form-group custom-select-wrapper">
                                            <select name="contact_subject" className="form-control" id="contactSubject">
                                                <option value="Please Select">Please Select</option>
                                                <option value="Sales">Sales</option>
                                                <option value="Services">Services</option>
                                                <option value="Accounting">Accounting</option>
                                            </select>
                                            <i className="far fa-bookmark"></i>
                                        </div>
                                    </div>
                                    <div className="col-lg-12">
                                        <div className="contact-form-group">
                                            <textarea name="contact_message" id="contactMessage" className="form-control"  placeholder="Your Message *" cols="20" rows="7"></textarea>
                                            <i className="far fa-envelope-open"></i>
                                            <div className="form-validate-icons">
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div className="col-lg-12">
                                        <div className="contact-btn-left">
                                            <button type="submit" id="contactBtn" className="default-button">Send Message</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <script src="https://use.fontawesome.com/923c380dfe.js"></script>

        </div>
    )
}

export default index
