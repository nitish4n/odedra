import React, { useState, useLayoutEffect } from 'react'
import './home.css'
import HomeBanner from '../../Component/Home/home';
import Service from '../../Component/Services/Service'
import LabelImportantIcon from '@material-ui/icons/LabelImportant';
import Team from '../../Component/Team';
function Home() {
    
    return (
        <div className="home">
            {/* <img 
            src="https://res.cloudinary.com/nitish4n/image/upload/v1600464525/vijay/mixkit-woman-dressed-in-traditional-chinese-clothing-holding-a-festival-lantern-98-desktop-wallpaper_qv4wsz.png"
            alt=""
            /> */}
            <HomeBanner />
            <div id="services" className="section page-split">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="bg-holder float-left">
                            <div className="split-color"></div>                                
                        </div>
                        <div className="sticky-spacer">
                            <div className="section-title-holder float-left">
                                <div className="section-top-image"> 
                                    <img src="images/01_left.png" alt="" />
                                </div>
                                <h2 className="entry-title">
                                    WHAT <br />
                                    WE <br />
                                    DO
                                </h2>
                                <p className="page-desc">
                                    Our modest <br />
                                    list of services
                                </p>
                            </div>
                        </div>   

                        <div className="section-content-holder float-right">
                            <div className="content-wrapper">
                                <div className="info-text">Our modest list of services to <br /> suit all your digital needs</div>
                                <p>&nbsp;</p>

                                <div className="one_half animate">
                                    <div className="service-holder">
                                        <div className="service-img"> 
                                            <img src="images/icon_preciese.png" alt="" />
                                        </div>
                                        <div className="service-txt">
                                            <h4>Animation Videos</h4> 
                                            Phasellus vel est sem integer suscipit enim quis dictum.
                                        </div>
                                    </div>
                                </div>

                                <div className="one_half last animate">
                                    <div className="service-holder">
                                        <div className="service-img">
                                            <img src="images/icon_support.png" alt="" />
                                        </div>
                                        <div className="service-txt">
                                            <h4>Development</h4> 
                                            Est sem integer suscipit enim quis dictum feugiat curabitur.
                                        </div>
                                    </div>
                                </div>
                                <div className="clear"></div>

                                <div className="one_half animate">
                                    <div className="service-holder">
                                        <div className="service-img">
                                            <img src="images/icon_responsive.png" alt="" />
                                        </div>
                                        <div className="service-txt">
                                            <h4>Digital Marketing</h4> 
                                            Donec vel est sem integer suscipit enim quis lorem.
                                        </div>
                                    </div>
                                </div>

                                <div className="one_half last animate">
                                    <div className="service-holder">
                                        <div className="service-img"> 
                                            <img src="images/icon_community.png" alt="" />
                                        </div>
                                        <div className="service-txt">
                                            <h4>Graphics Designing</h4> 
                                            Lorem integer suscipit enim quis dictum feugiat donec.
                                        </div>
                                    </div>
                                </div>
                                <div className="clear"></div>

                                <div className="one_half animate">
                                    <div className="service-holder">
                                        <div className="service-img"> 
                                            <img src="images/icon_portfolio.png" alt="" />
                                        </div>
                                        <div className="service-txt">
                                            <h4>Branding</h4> 
                                            Amet vel est sem integer suscipit enim quis dolor.
                                        </div>
                                    </div>
                                </div>

                                <div className="one_half last animate">
                                    <div className="service-holder">
                                        <div className="service-img">
                                            <img src="images/icon_store.png" alt="" />
                                        </div>
                                        <div className="service-txt">
                                            <h4>Website</h4> 
                                            Sem integer suscipit enim quis dictum feugiat suscipit.
                                        </div>
                                    </div>
                                </div>
                                <div className="clear"></div>
                            </div>                                                           
                        </div>                                
                        <div className="clear"></div> 
                    </div>
                </div>
        
                <div id="portfolio" className="section page-split">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="bg-holder float-left">
                            <div className="split-color"></div>                                   
                        </div>
                        <div className="sticky-spacer">
                            <div className="section-title-holder float-left portfolio-title-fix-class">
                                <div className="section-top-image"> 
                                    <img src="images/02_left.png" alt="" />
                                </div>
                                <h2 className="entry-title"> 
                                    PORT<br />FOL<br />IO
                                </h2>
                                <p className="page-desc"> 
                                    Check our <br />
                                    recent work
                                </p>
                            </div>
                        </div>

                        <div className="section-content-holder float-right">
                            <div className="content-wrapper">
                                <div id="portfolio-wrapper">
                                    <div className="portfolio-load-content-holder"></div>
                                    <div className="grid" id="portfolio-grid">                                            
                                        <div className="grid-sizer"></div>                                            

                                        <div id="p-item-1" className="grid-item element-item p_one">
                                            <a className="item-link ajax-portfolio" href="portfolio-1.html" data-id="1">
                                                <img src="images/img_01s.jpg" alt="" />
                                                <div className="portfolio-text-holder">
                                                    <p className="portfolio-title">Smartphone</p>
                                                    <p className="portfolio-desc">BUSINESS CARDS</p>
                                                </div>
                                            </a>
                                        </div>

                                        <div id="p-item-2" className="grid-item element-item p_one_half">
                                            <a className="item-link ajax-portfolio" href="portfolio-2.html" data-id="2">
                                                <img src="images/img_02s.jpg" alt="" />
                                                <div className="portfolio-text-holder">
                                                    <p className="portfolio-title">At Work</p>
                                                    <p className="portfolio-desc">PSD MOCKUP</p>
                                                </div>
                                            </a>
                                        </div>

                                        <div id="p-item-3" className="grid-item element-item p_one_half">
                                            <a className="item-link ajax-portfolio" href="portfolio-3.html" data-id="3">
                                                <img src="images/img_10s.jpg" alt="" />
                                                <div className="portfolio-text-holder">
                                                    <p className="portfolio-title">Carbon Bike</p>
                                                    <p className="portfolio-desc">ULTRAFAST</p>
                                                </div>
                                            </a>
                                        </div>

                                        <div className="grid-item element-item p_one_half">
                                            <a className="item-link" href="images/img_05s.jpg" data-rel="prettyPhoto[portfolio]">
                                                <img src="images/img_11s.jpg" alt="" />
                                                <div className="portfolio-text-holder">
                                                    <p className="portfolio-title">Tower</p>
                                                    <p className="portfolio-desc">SKYSCRAPER</p>
                                                </div>
                                            </a>
                                        </div>

                                        <div className="grid-item element-item p_one">
                                            <a className="item-link" href="https://www.youtube.com/watch?v=15cpIHjEsWI" data-rel="prettyPhoto[portfolio]">
                                                <img src="images/img_12s.jpg" alt="" />
                                                <div className="portfolio-text-holder">
                                                    <p className="portfolio-title">A4 Paper</p>
                                                    <p className="portfolio-desc">PSD MOCKUP</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div className="clear"></div>                                            
                                </div>
                                <div className="block center-relative center-text more-posts-portfolio-holder">
                                    <a className="more-posts-portfolio">
                                        <img src="images/icon_plus.png" alt="" />
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="clear"></div>                                
                    </div>                            
                </div>

                <div id="inspire" className="section full-screen full-width">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="content-wrapper">
                            <script>
                                var slider1_speed = "2000";
                                var slider1_auto = "true";
                                var slider1_hover = "true";                                
                            </script>
                            <div className="image-slider-wrapper relative">
                                <div id="slider1" className="owl-carousel owl-theme image-slider slider">
                                    <div className="owl-item">
                                        <a href="#services" className="scroll">
                                            <img src="images/img_01.png" alt="" />
                                        </a>
                                    </div>
                                    <div className="owl-item">
                                        <a href="#team" className="scroll">
                                            <img src="images/img_02.png" alt="" />
                                        </a>
                                    </div>                               
                                    <div className="owl-item">
                                        <a href="#contact" className="scroll">
                                            <img src="images/img_03.png" alt="" />
                                        </a>
                                    </div>
                                </div>                                                            
                            </div>
                        </div>
                    </div>
                </div>


                <div id="about" className="section page-split">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="bg-holder float-right">
                            <div className="split-color"></div>                                    
                        </div>
                        <div className="sticky-spacer">
                            <div className="section-title-holder float-right">
                                <div className="section-top-image">
                                    <img src="images/03_right.png" alt="" />
                                </div>
                                <h2 className="entry-title"> 
                                    WHO <br />WE <br />ARE
                                </h2>
                                <p className="page-desc"> 
                                    About our <br />
                                    small company
                                </p>
                            </div>
                        </div>

                        <div className="section-content-holder float-left">
                            <div className="content-wrapper">
                                <div className="info-text">
                                    Two ghostly white figures in coveralls and helmets are soflty dancing. Shores of the cosmic ocean permanence of the stars.
                                </div>
                                <p>&nbsp;</p> 
                                <a className="video-popup-holder" href="https://www.youtube.com/watch?v=15cpIHjEsWI" data-rel="prettyPhoto[gallery-video1]">
                                    <img className="thumb" src="images/video_img.jpg" alt="" />
                                    <img className="popup-play" src="images/play_btn.png" alt="Play" />
                                </a>
                                <p>&nbsp;</p>

                                <div className="one_half">
                                    <p>
                                        <strong>As a patch of light. Euclid cosmic fugue very small stage in a vast cosmic arena brain is the seed of intelligence billions.</strong>
                                    </p>
                                    <p>&nbsp;</p>
                                    <p>
                                        Flatland! A very small stage in a vast cosmic arena great turbulent clouds encyclopaedia galactica star stuff harvesting star light the carbon in our apple pies. Realm of the galaxies, Cambrian explosion Flatland for tesserac
                                    </p>
                                </div>
                                <div className="one_half last">
                                    A very small stage in a vast cosmic arena great turbulent clouds encyclopaedia galactica star stuff harvesting star light the carbon in our apple pies. Realm of the galaxies, Cambrian explosion Flatland for tesseract hundreds of thousands, cosmic ocean. Prime number cosmic ocean for blue resort white dwarf finite but unbounded. A very small stage in a vast cosmic arena great turbulent clouds encyclopaedia galactica star stuff harvesting star light the carbon
                                </div>
                                <div className="clear"></div>                                       
                            </div>                                   
                        </div>
                        <div className="clear"></div>                                
                    </div>                            
                </div>


                <div id="news" className="section page-split">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="bg-holder float-right">
                            <div className="split-color"></div>                                    
                        </div>
                        <div className="sticky-spacer">
                            <div className="section-title-holder float-right">
                                <div className="section-top-image">
                                    <img src="images/04_right.png" alt="" />
                                </div>
                                <h2 className="entry-title"> 
                                    BL<br />OG
                                </h2>
                                <p className="page-desc"> 
                                    Read our <br />
                                    recent news
                                </p>
                            </div>
                        </div>

                        <div className="section-content-holder float-left">
                            <div className="content-wrapper">
                                <div className="blog-holder-scode latest-posts-scode block center-relative">
                                    <article className="relative blog-item-holder-scode">
                                        <div className="entry-date published">January 27, 2018</div>
                                        <div className="cat-links">
                                            <ul>
                                                <li>
                                                    <a href="#">Uncategorized</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <h4 className="entry-title">
                                            <a href="#">
                                                Globular star cluster radio scope great turbulent 
                                                <span className="arrow"></span>
                                            </a>
                                        </h4>
                                        <div className="excerpt">
                                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture, hydrogen atoms cosmic fugue across ...
                                        </div>
                                    </article>

                                    <article className="relative blog-item-holder-scode">
                                        <div className="entry-date published">January 26, 2018</div>
                                        <div className="cat-links">
                                            <ul>
                                                <li>
                                                    <a href="#">Uncategorized</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <h4 className="entry-title">
                                            <a href="#">
                                                Muse about a very small stage in a vast arena 
                                                <span className="arrow"></span>
                                            </a>
                                        </h4>
                                        <div className="excerpt">
                                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture, hydrogen atoms cosmic fugue across ...
                                        </div>
                                    </article>

                                    <article className="relative blog-item-holder-scode">
                                        <div className="entry-date published">January 25, 2018</div>
                                        <div className="cat-links">
                                            <ul>
                                                <li>
                                                    <a href="#">Uncategorized</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <h4 className="entry-title">
                                            <a href="#">
                                                Are creatures of the vast cosmos of global death 
                                                <span className="arrow"></span>
                                            </a>
                                        </h4>
                                        <div className="excerpt">
                                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture, hydrogen atoms cosmic fugue across ...
                                        </div>
                                    </article>

                                    <article className="relative blog-item-holder-scode">
                                        <div className="entry-date published">January 24, 2018</div>
                                        <div className="cat-links">
                                            <ul>
                                                <li>
                                                    <a href="#">Uncategorized</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <h4 className="entry-title">
                                            <a href="#">
                                                Preserve and cherish that pale blue dot star stuff 
                                                <span className="arrow"></span>
                                            </a>
                                        </h4>
                                        <div className="excerpt">
                                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture, hydrogen atoms cosmic fugue across ...
                                        </div>
                                    </article>

                                    <article className="relative blog-item-holder-scode">
                                        <div className="entry-date published">January 23, 2018</div>
                                        <div className="cat-links">
                                            <ul>
                                                <li>
                                                    <a href="#">Uncategorized</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <h4 className="entry-title">
                                            <a href="#">
                                                Kindling the energy hidden in matter vanquish now 
                                                <span className="arrow"></span>
                                            </a>
                                        </h4>
                                        <div className="excerpt">
                                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture, hydrogen atoms cosmic fugue across ...
                                        </div>
                                    </article>

                                    <article className="relative blog-item-holder-scode">
                                        <div className="entry-date published">January 22, 2018</div>
                                        <div className="cat-links">
                                            <ul>
                                                <li>
                                                    <a href="#">Uncategorized</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <h4 className="entry-title">
                                            <a href="#">
                                                Galaxyrise great turbulent clouds colonies 
                                                <span className="arrow"></span>
                                            </a>
                                        </h4>
                                        <div className="excerpt">
                                            Intelligent beings. Rogue Jean-Francois Champollion. Hearts of the stars venture, hydrogen atoms cosmic fugue across ...
                                        </div>
                                    </article>
                                    <div className="clear"></div>                                            
                                </div>                                       
                            </div>                                    
                        </div>
                        <div className="clear"></div>                              
                    </div>                            
                </div>

                <div id="pricing" className="section full-width">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="content-wrapper">
                            <div className="two_third">
                                <div className="info-text"> 
                                    Terre estere doloremique sei totames laudantium remeseo aperiam, eaque ipsa quae ab illo inventore veritatis
                                </div>                                   
                            </div>
                            <div className="one_third last"> 
                                Dolor lorem ipsu seti per se neque porro quisquam est, quister set dolorem per sei donec nulla est rete et quasi architecto beataes vitae dicta sunt
                            </div>
                            <div className="clear"></div>

                            <div className="one_third">
                                <div className="pricing-table">
                                    <div className="pricing-table-header">
                                        <div className="pricing-table-title pricing-orange">
                                            BASIC
                                        </div>                                           
                                    </div>
                                    <div className="pricing-table-price">$29</div>
                                    <div className="pricing-table-desc">per month</div>
                                    <div className="pricing-table-content-holder">
                                        <ul>
                                            <li>25 GB Storage</li>
                                            <li>24/7 Support</li>
                                            <li>Up to 50 Users</li>
                                            <li>Donec Estu</li>
                                            <li>Lorem Ipsum</li>
                                        </ul>
                                    </div>
                                    <a href="#" className="pricing-button scroll">CHOOSE</a>
                                </div>
                            </div>

                            <div className="one_third">
                                <div className="pricing-table">
                                    <div className="pricing-table-header">
                                        <div className="pricing-table-title pricing-blue">
                                            ADVANCED
                                        </div>                                           
                                    </div>
                                    <div className="pricing-table-price">$59</div>
                                    <div className="pricing-table-desc">per month</div>
                                    <div className="pricing-table-content-holder">
                                        <ul>
                                            <li>25 GB Storage</li>
                                            <li>24/7 Support</li>
                                            <li>Up to 50 Users</li>
                                            <li>Donec Estu</li>
                                            <li>Lorem Ipsum</li>
                                        </ul>
                                    </div>
                                    <a href="#" className="pricing-button scroll">CHOOSE</a>
                                </div>
                            </div>
                            <div className="one_third last">
                                <div className="pricing-table">
                                    <div className="pricing-table-header">
                                        <div className="pricing-table-title pricing-green">
                                            EXPERT
                                        </div>                                            
                                    </div>
                                    <div className="pricing-table-price">$99</div>
                                    <div className="pricing-table-desc">per month</div>
                                    <div className="pricing-table-content-holder">
                                        <ul>
                                            <li>25 GB Storage</li>
                                            <li>24/7 Support</li>
                                            <li>Up to 50 Users</li>
                                            <li>Donec Estu</li>
                                            <li>Lorem Ipsum</li>
                                        </ul>
                                    </div>
                                    <a href="#" className="pricing-button scroll">CHOOSE</a>
                                </div>
                            </div>
                            <div className="clear"></div>                               
                        </div>
                        <div className="clear"></div>                            
                    </div>                       
                </div>


                <div id="skills" className="section page-split">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="bg-holder float-left">
                            <div className="split-color"></div>                               
                        </div>
                        <div className="sticky-spacer">
                            <div className="section-title-holder float-left">
                                <div className="section-top-image"> 
                                    <img src="images/05_left.png" alt="" />
                                </div>
                                <h2 className="entry-title"> 
                                    OUR <br />SKI<br />LLS
                                </h2>
                                <p className="page-desc">
                                    Our set of <br />
                                    competences
                                </p>
                            </div>
                        </div>
                        <div className="section-content-holder float-right">
                            <div className="content-wrapper">
                                <div className="info-text">
                                    Ghostly white figures in coveralls and <br /> 
                                    helmets are soflty dancing of the cosmic
                                </div>
                                <p>&nbsp;</p>

                                <div className="skills-holder">
                                    <div className="skill-holder">
                                        <div className="skill-percent">75%</div>
                                        <div className="skill-text">
                                            <span>Creativity</span>
                                            <div className="skill">
                                                <div className="skill-fill" data-fill="75%"></div>                                                    
                                            </div>                                               
                                        </div>                                            
                                    </div>

                                    <div className="skill-holder">
                                        <div className="skill-percent">45%</div>
                                        <div className="skill-text">
                                            <span>Cooking</span>
                                            <div className="skill">
                                                <div className="skill-fill" data-fill="45%"></div>                                                   
                                            </div>                                               
                                        </div>                                            
                                    </div>

                                    <div className="skill-holder">
                                        <div className="skill-percent">90%</div>
                                        <div className="skill-text">
                                            <span>PhP</span>
                                            <div className="skill">
                                                <div className="skill-fill" data-fill="90%"></div>                                                   
                                            </div>                                                
                                        </div>                                            
                                    </div>

                                    <div className="skill-holder">
                                        <div className="skill-percent">65%</div>
                                        <div className="skill-text">
                                            <span>Marketing</span>
                                            <div className="skill">
                                                <div className="skill-fill" data-fill="65%"></div>                                                   
                                            </div>                                                
                                        </div>                                            
                                    </div>

                                    <div className="skill-holder">
                                        <div className="skill-percent">85%</div>
                                        <div className="skill-text">
                                            <span>Design</span>
                                            <div className="skill">
                                                <div className="skill-fill" data-fill="85%"></div>                                                    
                                            </div>                                                
                                        </div>                                            
                                    </div>                                        
                                </div>                                    
                            </div>                                
                        </div>
                        <div className="clear"></div>                            
                    </div>                        
                </div>

                <div id="team" className="section page-split">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="bg-holder float-left">
                            <div className="split-color"></div>                                
                        </div>
                        <div className="sticky-spacer">
                            <div className="section-title-holder float-left">
                                <div className="section-top-image"> 
                                    <img src="images/06_left.png" alt="" />
                                </div>
                                <h2 className="entry-title"> 
                                    MEET <br />OUR <br />TEAM
                                </h2>
                                <p className="page-desc">
                                    Get to know us <br />
                                    much better
                                </p>
                            </div>
                        </div>

                        <div className="section-content-holder float-right">
                            <div className="content-wrapper">
                                <div id="team-holder">
                                    <div className="team-load-content-holder"></div>
                                    <ul className="member-holder-wrapper">

                                        <li id="team-member-1" className="member-holder one_half">
                                            <a className="img-link ajax-member-content" href="team-1.html" data-id="1">
                                                <img className="attachment-post-thumbnail" src="images/team_01_large.jpg" alt="" />
                                                <div className="member-mask">
                                                    <div className="member-info-holder">
                                                        <p className="member-name">Mark Stone</p>
                                                        <p className="member-position">CEO</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li id="team-member-2" className="member-holder one_half">
                                            <a className="img-link ajax-member-content" href="team-2.html" data-id="2">
                                                <img className="attachment-post-thumbnail" src="images/team_02_large.jpg" alt="" />
                                                <div className="member-mask">
                                                    <div className="member-info-holder">
                                                        <p className="member-name">Kate Stonehold</p>
                                                        <p className="member-position">DESIGNER</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li id="team-member-3" className="member-holder one_half">
                                            <a className="img-link ajax-member-content" href="team-3.html" data-id="3">
                                                <img className="attachment-post-thumbnail" src="images/team_03_large.jpg" alt="" />
                                                <div className="member-mask">
                                                    <div className="member-info-holder">
                                                        <p className="member-name">Terry Jackobs</p>
                                                        <p className="member-position">CODER</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li id="team-member-4" className="member-holder one_half">
                                            <a className="img-link ajax-member-content" href="team-4.html" data-id="4">
                                                <img className="attachment-post-thumbnail" src="images/team_04_large.jpg" alt="" />
                                                <div className="member-mask">
                                                    <div className="member-info-holder">
                                                        <p className="member-name">Jane Perry</p>
                                                        <p className="member-position">HR</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li id="team-member-5" className="member-holder one_half">
                                            <a className="img-link ajax-member-content" href="team-5.html" data-id="5">
                                                <img className="attachment-post-thumbnail" src="images/team_05_large.jpg" alt="" />
                                                <div className="member-mask">
                                                    <div className="member-info-holder">
                                                        <p className="member-name">Peter Williams</p>
                                                        <p className="member-position">DESIGNER</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li id="team-member-6" className="member-holder one_half">
                                            <a className="img-link ajax-member-content" href="team-6.html" data-id="6">
                                                <img className="attachment-post-thumbnail" src="images/team_06_large.jpg" alt="" />
                                                <div className="member-mask">
                                                    <div className="member-info-holder">
                                                        <p className="member-name">John Doe</p>
                                                        <p className="member-position">MARKETING</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <div className="clear"></div>                                        
                                </div>                                    
                            </div>                                
                        </div>
                        <div className="clear"></div>                           
                    </div>                        
                </div>


                <div id="clients" className="section full-width">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="content-wrapper">
                            <div className="one_half">
                                <script>
                                    var textSlider1_speed = "2000";
                                    var textSlider1_auto = "true";
                                    var textSlider1_hover = "true";
                                </script>
                                <div className="text-slider-wrapper relative">
                                    <div className="text-slider-header-quotes"></div>

                                    <div id="textSlider1" className="text-slider slider owl-carousel owl-theme">

                                        <div className="text-slide">
                                            <p className="text-slide-content">
                                                A very small stage in a vast cosmic arena are creatures of the cosmos vanquish the impossible white dwarf astonishment. A still more glorious dawn awaits light.
                                            </p>
                                            <img className="text-slide-img" src="images/testiomonial_img_01.jpg" alt="" />
                                            <p className="text-slide-name">Peter Hall</p>
                                            <p className="text-slide-position">CEO</p>                                            
                                        </div>   

                                        <div className="text-slide">
                                            <p className="text-slide-content">
                                                Globular star cluster, another world brain is the seed of intelligence a billion trillion bits of moving fluff hearts of the stars. Hypatia preserve and cherish that pale blue dot.
                                            </p>
                                            <img className="text-slide-img" src="images/testiomonial_img_02.jpg" alt="" />
                                            <p className="text-slide-name">Jane Williams</p>
                                            <p className="text-slide-position">DESIGNER</p>                                            
                                        </div>   

                                        <div className="text-slide">
                                            <p className="text-slide-content">
                                                Hypatia preserve and cherish that pale blue dot. Hundreds of thousands! Inconspicuous motes of rock and gas the carbon in our apple pies creatures of the cosmos.
                                            </p>
                                            <img className="text-slide-img" src="images/testiomonial_img_03.jpg" alt="" />
                                            <p className="text-slide-name">John Smith</p>
                                            <p className="text-slide-position">GURU</p>                                            
                                        </div>   

                                    </div>

                                    <div className="clear"></div>
                                </div>                                    
                            </div>


                            <div className="one_fourth top-80"> 
                                <img src="images/client_logo_01.png" alt="" className="bottom-25 aligncenter" />                                
                                <img src="images/client_logo_05.png" alt="" className="bottom-25 aligncenter" />                                
                                <img src="images/client_logo_02.png" alt="" className="bottom-25 aligncenter" />                                
                            </div>

                            <div className="one_fourth last top-80">
                                <img src="images/client_logo_06.png" alt="" className="bottom-25 aligncenter" />                                
                                <img src="images/client_logo_03.png" alt="" className="bottom-25 aligncenter" />                                
                                <img src="images/client_logo_04.png" alt="" className="bottom-25 aligncenter" />                                
                            </div>
                            <div className="clear"></div>                                
                        </div>
                        <div className="clear"></div>                           
                    </div>                        
                </div>

                <div id="contact" className="section page-split">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="bg-holder float-left">
                            <div className="split-color"></div>                               
                        </div>
                        <div className="sticky-spacer">
                            <div className="section-title-holder float-left">
                                <div className="section-top-image"> 
                                    <img src="images/07_left.png" alt="" />
                                </div>
                                <h2 className="entry-title"> 
                                    STAY <br />IN <br />TOUCH
                                </h2>
                                <p className="page-desc"> 
                                    Use contact <br />
                                    form on the right
                                </p>
                            </div>
                        </div>

                        <div className="section-content-holder float-right">
                            <div className="content-wrapper">
                                <div className="info-text">
                                    Ghostly white figures in coveralls and <br /> 
                                    helmets are soflty dancing of the cosmic
                                </div>
                                <p>&nbsp;</p>

                                <div className="contact-form">
                                    <p><input id="name" type="text" name="your-name" placeholder="Name" /></p>
                                    <p><input id="contact-email" type="email" name="your-email" placeholder="Email" /></p>                                    
                                    <p><input id="subject" type="text" name="your-subject" placeholder="Subject" /></p>
                                    <p><textarea id="message" name="your-message" placeholder="Message"></textarea></p>
                                    <p className="contact-submit-holder"><input type="submit" onClick="SendMail()" value="SEND" /></p>                                
                                </div>  

                                <p>&nbsp;</p>

                                <div className="one_half"> 
                                    Flatland! A very small stage in a vast cosmic arena great turbulent clouds encyclopaedia galactica star stuff harvesting
                                </div>

                                <div className="one_half last"> 
                                    Phone: +321 123 456 7 <br /> 
                                    E-mail: hello@cocobasic.com <br /> 
                                    Website: www.yoursitegoeshere.com <br />
                                </div>
                                <div className="clear"></div>                                   
                            </div>                                
                        </div>
                        <div className="clear"></div>                            
                    </div>                        
                </div>

  
                <div id="milestones" className="section full-width">
                    <div className="section-wrapper block content-1170 center-relative">
                        <div className="content-wrapper">
                            <ul className="milestones-holder">
                                <li className="milestone">
                                    <p className="milestone-num">35</p>
                                    <p className="milestone-text">
                                        Terabytes of <br /> 
                                        files uploaded
                                    </p>
                                </li>
                                <li className="milestone">
                                    <p className="milestone-num">67K</p>
                                    <p className="milestone-text">
                                        Lines of CSS code wrote
                                    </p>
                                </li>
                                <li className="milestone">
                                    <p className="milestone-num">74</p>
                                    <p className="milestone-text">
                                        Cups of coffee drinked per week
                                    </p>
                                </li>
                                <li className="milestone">
                                    <p className="milestone-num">389</p>
                                    <p className="milestone-text">
                                        Happy <br /> 
                                        clients served
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <div className="clear"></div>                           
                    </div>                       
                </div>                    
        </div>
    )
}

export default Home
