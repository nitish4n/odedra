import React, {useLayoutEffect, useState} from 'react';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Header from './Component/Header/Header';
import Footer from './Component/Footer/Footer';
import Home from './Container/Home/Home';
import Contact from './Container/ContactUs';
import About from './Container/About/index';
import Services from './Container/Services';
function App() {
  const [scrollPosition, setPosition] = useState(0);
  const [changeNav, setChangeNav] = useState('yes');

    useLayoutEffect(() => {
        function updatePosition() {
            setPosition(window.pageYOffset);
            setChangeNav(`okay ${scrollPosition}`)
        }
        window.addEventListener('scroll', updatePosition);
            updatePosition();
        }, [])

  return (
    <div className="App">
      <Router>
        <Header scrollProperty={scrollPosition} />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about-us"  component={About} />
          <Route path="/contact-us"  component={Contact} />
          <Route path="/services"  component={Services} />
          
        </Switch>

        <Footer />
      </Router>
      
    </div>
  );
}

export default App;
